package com.example.bibliotheque;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

public class LoginController {
    @FXML
    private TextField mail;
    @FXML
    private PasswordField password;
    private BDDHandler bdd;
    public void setBdd(BDDHandler bdd)
    {
        this.bdd = bdd;
    }
    @FXML
    protected void connectionBouton() throws SQLException, IOException {
        User user = bdd.connectUser(mail.getText(),password.getText());
        if (user == null)
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Mauvaise identification");
            alert.setHeaderText("Erreur");
            alert.setContentText("Couple mail / mot de passe invalide, ou utilisateur banni");//from   www  .  ja va  2 s  .com
            alert.showAndWait();
        }
        else if (user.getAdmin()){
            AdminController adminController = new AdminController();
            adminController.setBdd(bdd);
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setController(adminController);
            fxmlLoader.setLocation(BiblioApp.class.getResource("admin.fxml"));
            Stage stage = (Stage) mail.getScene().getWindow();
            Scene scene = new Scene(fxmlLoader.load());
            stage.setTitle("Administration");
            stage.setScene(scene);
            stage.show();
        }
        else {
            UserController userController = new UserController();
            userController.setBdd(bdd);
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setController(userController);
            fxmlLoader.setLocation(BiblioApp.class.getResource("adherent.fxml"));
            Scene scene = new Scene(fxmlLoader.load());
            Stage stage = (Stage) mail.getScene().getWindow();
            stage.setTitle("Bienvenue à la bilbliothèque Marcel Pagnol !");
            stage.setScene(scene);
            stage.show();
        }
    }
}