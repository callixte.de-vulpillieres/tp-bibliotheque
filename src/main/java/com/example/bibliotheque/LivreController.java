package com.example.bibliotheque;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

import java.sql.SQLException;

public class LivreController {
    @FXML
    private ImageView couverture;
    @FXML
    private Text titre;
    @FXML
    private Text auteurs;
    @FXML
    private Text annee;
    @FXML
    private Text langue;
    @FXML
    private Text motsClefs;
    @FXML
    private Text synopsis;
    @FXML
    private TableView<Edition> exemplaires;
    @FXML
    private TableColumn<Edition, String> cEdition;
    @FXML
    private TableColumn<Edition, String> cAnnee;
    @FXML
    private TableColumn<Edition, String> cPages;
    @FXML
    private TableColumn<Edition, String> cLangue;
    @FXML
    private TableColumn<Edition, Boolean> cPoche;
    private Oeuvre oeuvre;
    private BDDHandler bdd;
    public void initialize() throws SQLException {
        this.titre.setText(this.oeuvre.getTitre());
        this.annee.setText("Publié en " + this.oeuvre.getAnnee().toString());
        this.auteurs.setText("De : " + this.oeuvre.getAuteurs());
        this.langue.setText("Langue originale : "+this.oeuvre.getLangue());
        this.motsClefs.setText("Mot-clefs : " + this.oeuvre.getMotsClefs());
        this.synopsis.setText(this.oeuvre.getSynopsis());

        this.cAnnee.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getAnnee().toString()));
        this.cEdition.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getEditeur()));
        this.cLangue.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getLangue()));
        this.cPoche.setCellValueFactory(cellData -> new SimpleBooleanProperty(cellData.getValue().getPoche()));
        this.cPages.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getPages())));
        this.loadTable();
    }
    private void loadTable() throws SQLException {
        ObservableList<Edition> exemplaires= bdd.editions(this.oeuvre);
        this.exemplaires.setItems(exemplaires);
    }
    @FXML
    public void emprunter() throws SQLException {
        if (bdd.emprunter(this.exemplaires.getSelectionModel().getSelectedItem())) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Réservation confirmée");
            alert.setContentText("Votre emprunt est bien enregistré");
            alert.showAndWait();
            loadTable();
        }
        else
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Réservation en échec");
            alert.setContentText("Votre emprunt n'a pas pu être effectué");
            alert.showAndWait();
        }
    }
    public void creer(BDDHandler bdd, Oeuvre oeuvre)
    {
        this.bdd = bdd;
        this.oeuvre = oeuvre;
    }
}
