package com.example.bibliotheque;

import java.time.LocalDate;

public class Ban {
    private User bannisseur;
    private LocalDate debut;
    private LocalDate fin;
    private String motif;
    public Ban(LocalDate debut, LocalDate fin, String motif, User bannisseur)
    {
        this.bannisseur = bannisseur;
        this.debut = debut;
        this.fin = fin;
        this.motif = motif;
    }


    public String getBans() {
        return "Par : " + bannisseur.getPrenom() + " " + bannisseur.getNom() + ", du "+debut.toString() + " au "+ fin.toString() + ". Motif : " + motif;
    }
}
