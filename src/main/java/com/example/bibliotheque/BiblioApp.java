package com.example.bibliotheque;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

public class BiblioApp extends Application {
    @Override
    public void start(Stage stage) throws IOException, SQLException {
        BDDHandler bdd = new BDDHandler();
        FXMLLoader fxmlLoader = new FXMLLoader(BiblioApp.class.getResource("login.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 320, 240);
        stage.setTitle("Login");
        stage.setScene(scene);
        LoginController loginController = fxmlLoader.<LoginController>getController();
        loginController.setBdd(bdd);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}