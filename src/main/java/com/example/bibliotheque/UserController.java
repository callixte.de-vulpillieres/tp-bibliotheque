package com.example.bibliotheque;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableStringValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.Year;

import java.sql.SQLException;

public class UserController {
    private BDDHandler bdd;
    @FXML
    private TableView<Emprunt> emprunts;
    @FXML
    private TableColumn<Emprunt,String> ctitre;
    @FXML
    private TableColumn<Emprunt,String> cauteur;
    @FXML
    private TableColumn<Emprunt, String> cannee;
    @FXML
    private TableColumn<Emprunt,String> cediteur;
    @FXML
    private TableColumn<Emprunt,String> cisbn;
    @FXML
    private TextField rechercheTexte;
    @FXML
    private ChoiceBox<String> choix;
    @FXML
    private TableView<Oeuvre> resultatsRecherche;
    @FXML
    private TableColumn<Oeuvre,String> rTitre;
    @FXML
    private TableColumn<Oeuvre,String> rAuteur;
    @FXML
    private TableColumn<Oeuvre,String> rAnnee;
    @FXML
    private Button emprunter;
    @FXML
    private void rendre() throws SQLException {
        bdd.rendre(emprunts.getSelectionModel().getSelectedItem());
        loadTable();
    }
    public void initialize() throws SQLException {
        loadTable();
        this.choix.setItems(FXCollections.observableArrayList("Titre","Auteur","Mot-clef","Résumé","Éditeur"));
        this.choix.setValue("Titre");
    }
    private void loadTable() throws SQLException {
        ObservableList<Emprunt> emprunts = bdd.emprunts(true);
        this.ctitre.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTitre()));
        this.cisbn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getIsbn()));
        this.cannee.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getAnnee().toString()));
        this.cediteur.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getEditeur()));
        this.cauteur.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getAuteurs()));
        this.emprunts.setItems(emprunts);
    }
    public void setBdd(BDDHandler bdd)
    {
        this.bdd = bdd;
    }
    @FXML
    private void rechercher() throws SQLException {
        ObservableList<Oeuvre> resultats = switch (this.choix.getValue()) {
            case "Titre" -> bdd.rechercheTitre(this.rechercheTexte.getText());
            case "Auteur" -> bdd.rechercheAuteur(this.rechercheTexte.getText());
            case "Mot-clef" -> bdd.rechercheMotClef(this.rechercheTexte.getText());
            case "Résumé" -> bdd.rechercheResume(this.rechercheTexte.getText());
            case "Éditeur" -> bdd.rechercheEditeur(this.rechercheTexte.getText());
            default -> FXCollections.observableArrayList();
        };
        this.rTitre.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTitre()));
        this.rAnnee.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getAnnee().toString()));
        this.rAuteur.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getAuteurs()));
        this.resultatsRecherche.setItems(resultats);
    }
    @FXML
    public void emprunter() throws IOException, SQLException {
        Oeuvre oeuvre = this.resultatsRecherche.getSelectionModel().getSelectedItem();
        if (oeuvre == null)
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Pas d'oeuvre selectionnée");
            alert.setHeaderText("Erreur");
            alert.setContentText("Vous n'avez pas selectionné d'oeuvre");
            alert.showAndWait();
        }
        else {
            LivreController livreController = new LivreController();
            livreController.creer(bdd, oeuvre);
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setController(livreController);
            fxmlLoader.setLocation(BiblioApp.class.getResource("livre.fxml"));
            Scene scene = new Scene(fxmlLoader.load());
            Stage stage = new Stage();
            stage.setTitle("Bilbliothèque Marcel Pagnol !");
            stage.setScene(scene);
            stage.showAndWait();
            this.loadTable();
        }
    }
}
