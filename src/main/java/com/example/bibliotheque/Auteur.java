package com.example.bibliotheque;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Auteur {
    private static HashMap<Integer,Auteur> loaded = new HashMap<>();
    private String prenom ;
    private String nom;
    private LocalDate naissance;
    private int id;
    private Auteur(int id, String prenom, String nom, LocalDate naissance) {
        this.prenom = prenom;
        this.nom = nom;
        this.naissance = naissance;
        this.id = id;
    }

    public static Auteur get(int id, Connection con) throws SQLException {
        if (!loaded.containsKey(id))
        {
            PreparedStatement stmt = con.prepareStatement("Select prenom, nom, naissance from auteur where id= ?");
            stmt.setInt(1,id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next())
            {
                loaded.put(id, new Auteur(id,rs.getString(1),rs.getString(2), rs.getDate(3).toLocalDate()));
            }
        }
        return loaded.get(id);
    }

    public int getId() {
        return id;
    }
    public String toString()
    {
        return this.prenom + " " + this.nom;
    }
}
