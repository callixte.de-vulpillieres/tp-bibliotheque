package com.example.bibliotheque;

import java.sql.*;
import java.time.LocalDate;
import java.time.Year;
import java.util.HashMap;

public class Emprunt {
    private static HashMap<Integer,Emprunt> loaded = new HashMap<>();
    private int id;
    private User utilisateur;
    private LocalDate debut;
    private LocalDate finPrevue;
    private LocalDate fin = null;
    private Livre livre;
    private Emprunt(int id, User utilisateur, Livre livre, LocalDate debut, LocalDate finPrevue, LocalDate rendu)
    {
        this.utilisateur = utilisateur;
        this.livre = livre;
        this.debut = debut;
        this.finPrevue = finPrevue;
        this.fin = rendu;
        this.id = id ;
    }
    public static Emprunt get(int id, Connection con) throws SQLException {
        if (!loaded.containsKey(id))
        {
            PreparedStatement stmt = con.prepareStatement("Select id_utilisateur, id_livre, debut, fin_prevue, rendu from emprunt where id= ?");
            stmt.setInt(1,id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next())
            {
                Date retour = rs.getDate(5);
                LocalDate reto = null;
                if (retour != null)
                {
                    reto = retour.toLocalDate();
                }
                loaded.put(id, new Emprunt(id,User.get(rs.getInt(1),con),Livre.get(rs.getInt(2),con), rs.getDate(3).toLocalDate(),rs.getDate(4).toLocalDate(),reto));
            }
        }
        return loaded.get(id);
    }

    public static void set(User actif, Edition edition, Connection con) {

    }

    public String getIsbn()
    {
        return livre.getIsbn();
    }
    public int getIdLivre()
    {
        return livre.getId();
    }
    public String getTitre()
    {
        return livre.getTitre();
    }
    public void rendre() {
        fin = LocalDate.now();
    }

    public Year getAnnee() {
        return this.livre.getAnnee();
    }

    public String getEditeur() {
        return this.livre.getEditeur();
    }

    public String getAuteurs() {
        return this.livre.getAuteurs();
    }

    public String getUserName() {
        return this.utilisateur.getNomStr();
    }

    public String getRetour() {
        return this.finPrevue.toString();
    }
}
