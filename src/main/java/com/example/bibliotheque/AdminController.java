package com.example.bibliotheque;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.sql.SQLException;

public class AdminController {
    private BDDHandler bdd;
    @FXML
    private TableView<Emprunt> emprunts;
    @FXML
    private TableColumn<Emprunt,String> ctitre;
    @FXML
    private TableColumn<Emprunt,String> cauteur;
    @FXML
    private TableColumn<Emprunt, String> cuser;
    @FXML
    private TableColumn<Emprunt,String> crendu;
    @FXML
    private TableColumn<Emprunt,String> cisbn;
    @FXML
    private TextField rechercheTexte;
    @FXML
    private TableView<User> resultatsRecherche;
    @FXML
    private TableColumn<User,String> rPrenom;
    @FXML
    private TableColumn<User,String> rForfait;
    @FXML
    private TableColumn<User,String> rNom;
    @FXML
    private TableColumn<User,String> rBans;
    @FXML
    private DatePicker date;
    @FXML
    private TextField motif;
    @FXML
    private ChoiceBox<Forfait> forfait;
    @FXML
    private TableView<Forfait> fTable;
    @FXML
    private  TableColumn<Forfait,String> fNom;
    @FXML
    private TableColumn<Forfait,String> fDuree;
    @FXML
    private TableColumn<Forfait,String> fSimul;
    @FXML
    private TextField ftNom;
    @FXML
    private Spinner<Integer> ftDuree;
    @FXML
    private Spinner<Integer> ftSimul;
    private void loadTableEmprunts() throws SQLException {
        this.emprunts.refresh();
        ObservableList<Emprunt> emprunts = bdd.emprunts(false);
        this.emprunts.setItems(emprunts);
    }
    private void loadfTable() throws SQLException {
        this.fTable.refresh();
        ObservableList<Forfait> emprunts = bdd.forfaits();
        this.fTable.setItems(emprunts);
    }
    public void initialize() throws SQLException {
        this.fNom.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNom()));
        this.fDuree.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getDuree())));
        this.fSimul.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getDuree())));

        this.ctitre.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTitre()));
        this.cisbn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getIsbn()));
        this.cuser.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getUserName()));
        this.cauteur.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getAuteurs()));
        this.crendu.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getRetour()));

        loadTableEmprunts();
        loadfTable();
        SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 50, 10);
        SpinnerValueFactory<Integer> valueFactory2 = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 50, 10);

        ftDuree.setValueFactory(valueFactory);
        ftSimul.setValueFactory(valueFactory2);

        forfait.setItems(bdd.forfaits());
    }
    @FXML
    public void rechercher() throws SQLException {
        ObservableList<User> resultats =  bdd.rechercheUser(this.rechercheTexte.getText());
        this.rPrenom.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPrenom()));
        this.rNom.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNom()));
        this.rBans.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getBans()));
        this.rForfait.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getForfait()));
        this.resultatsRecherche.setItems(resultats);
    }
    @FXML
    public void bannir() throws SQLException {
        if (resultatsRecherche.getSelectionModel().getSelectedItem() != null && date.getValue() !=null && motif.getText() != "") {
            bdd.bannir(resultatsRecherche.getSelectionModel().getSelectedItem(), date.getValue(),motif.getText());
        }
        this.rechercher();
    }
    @FXML
    public void confirmerForfait() throws SQLException {
        if(resultatsRecherche.getSelectionModel().getSelectedItem() != null && forfait.getValue() != null)
        {
            resultatsRecherche.getSelectionModel().getSelectedItem().setForfait(forfait.getValue(),bdd.getCon());
        }
    }
    @FXML
    public void modifF() throws SQLException {
        if (fTable.getSelectionModel().getSelectedItem() != null && (ftNom.getText() != ""))
        {
            fTable.getSelectionModel().getSelectedItem().modif(ftNom.getText(),ftDuree.getValue(),ftSimul.getValue(),bdd.getCon());
        }
        loadfTable();
    }
    @FXML
    public void ajoutF() throws SQLException {
        if (ftNom.getText() != "")
        {
            bdd.addForfait(ftNom.getText(),ftDuree.getValue(),ftSimul.getValue());
        }
        loadfTable();
    }
    public void setBdd(BDDHandler bdd)
    {
        this.bdd = bdd;
    }
}
