package com.example.bibliotheque;

import javafx.beans.property.StringProperty;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Year;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Edition {
    private static HashMap<String,Edition> loaded = new HashMap<>();
    private String isbn;
    private Oeuvre oeuvre;
    private String editeur;
    private Year annee;
    private boolean poche;
    private int pages;
    private String langue;
    private String traducteurP;
    private String traducteurN;
    private Edition(String isbn, Oeuvre oeuvre, String editeur, Year annee, boolean poche, int pages, String langue, String traducteurP, String traducteurN)
    {
        this.oeuvre = oeuvre;
        this.isbn = isbn;
        this.editeur = editeur;
        this.annee = annee;
        this.poche = poche;
        this.pages = pages;
        this.traducteurN = traducteurN;
        this.traducteurP = traducteurP;
        this.langue = langue;
    }
    public static Edition get(String isbn, Connection con) throws SQLException {
        if (!loaded.containsKey(isbn))
        {
            PreparedStatement stmt = con.prepareStatement("Select id_oeuvre, editeur, annee, poche, pages, langue, traducteur_prenom, traducteur_nom from edition where isbn= ?");
            stmt.setString(1,isbn);
            ResultSet rs = stmt.executeQuery();
            if (rs.next())
            {
                loaded.put(isbn, new Edition(isbn,Oeuvre.get(rs.getInt(1),con),rs.getString(2), Year.of(rs.getInt(3)),rs.getBoolean(4),rs.getInt(5),rs.getString(6),rs.getString(7),rs.getString(8)));
            }
        }
        return loaded.get(isbn);
    }
    public String getIsbn()
    {
        return isbn;
    }

    public String getTitre() {
        return this.oeuvre.getTitre();
    }

    public Year getAnnee() {
        return this.annee;
    }

    public String getEditeur() {
        return this.editeur;
    }

    public String getAuteurs() {
        return this.oeuvre.getAuteurs();
    }

    public String getLangue() {
        return this.langue;
    }

    public boolean getPoche() {
        return  this.poche;
    }

    public int getPages() {
        return this.pages;
    }
}
