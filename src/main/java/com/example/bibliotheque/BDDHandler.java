package com.example.bibliotheque;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;
import java.time.LocalDate;

public class BDDHandler {
    private Connection con;
    private User actif;
    public BDDHandler() throws SQLException {
        con=DriverManager.getConnection("jdbc:mysql://localhost:3306/bibliotheque","root","password");
    }
    public User connectUser(String mail, String password) throws SQLException {
        PreparedStatement stmt = con.prepareStatement("SELECT id FROM utilisateur WHERE mail = ? and mot_de_passe = ? and NOT EXISTS (SELECT 1 FROM bans WHERE id_banni = id AND (CURRENT_DATE() BETWEEN debut AND fin))");
        stmt.setString(1,mail);
        stmt.setString(2,password);
        ResultSet rs = stmt.executeQuery();
        if(rs.next())
        {
            System.out.println(rs.getInt(1));
            actif = User.get(rs.getInt(1),con);
            return actif;
        }
        else {return null;}
    }

    public void rendre(Emprunt emprunt) throws SQLException {
        PreparedStatement stmt = con.prepareStatement("UPDATE emprunt SET rendu = CURRENT_DATE() WHERE rendu is null AND id_utilisateur = ? and id_livre = ?");
        stmt.setInt(1,actif.getId());
        stmt.setInt(2,emprunt.getIdLivre());
        stmt.executeUpdate();
        emprunt.rendre();
    }
    public ObservableList<Emprunt> emprunts(boolean current) throws SQLException {
        PreparedStatement stmt = null;
        if (current) {
            stmt = con.prepareStatement("SELECT id_livre FROM emprunt WHERE id_utilisateur = ? AND rendu IS null");
            stmt.setInt(1, actif.getId());
        }
        else
        {
            stmt = con.prepareStatement("SELECT id_livre FROM emprunt WHERE rendu IS null");
        }
        ResultSet rs = stmt.executeQuery();
        ObservableList<Emprunt>  res = FXCollections.observableArrayList();
        while(rs.next())
        {
             res.add(Emprunt.get(rs.getInt(1),con));
        }
        return res;
    }

    public ObservableList<Oeuvre> rechercheTitre(String text) throws SQLException {
        PreparedStatement stmt = con.prepareStatement("SELECT id FROM oeuvre WHERE titre LIKE ?");
        stmt.setString(1,"%"+text+"%");
        ResultSet rs = stmt.executeQuery();
        ObservableList<Oeuvre> res = FXCollections.observableArrayList();
        while(rs.next())
        {
            res.add(Oeuvre.get(rs.getInt(1),con));
        }
        return res;
    }

    public ObservableList<Oeuvre> rechercheAuteur(String text) throws SQLException {
        PreparedStatement stmt = con.prepareStatement("SELECT DISTINCT id_oeuvre FROM ecrit INNER JOIN auteur ON ecrit.id_auteur = auteur.id WHERE CONCAT(auteur.prenom, ' ', auteur.nom) LIKE ?");
        stmt.setString(1,"%"+text+"%");
        ResultSet rs = stmt.executeQuery();
        ObservableList<Oeuvre> res = FXCollections.observableArrayList();
        while(rs.next())
        {
            res.add(Oeuvre.get(rs.getInt(1),con));
        }
        return res;
    }

    public ObservableList<Oeuvre> rechercheMotClef(String text) throws SQLException {
        PreparedStatement stmt = con.prepareStatement("SELECT id FROM oeuvre WHERE CONCAT(motclef1,motclef2,motclef3,motclef4,motclef5) LIKE ?");
        stmt.setString(1,"%"+text+"%");
        ResultSet rs = stmt.executeQuery();
        ObservableList<Oeuvre> res = FXCollections.observableArrayList();
        while(rs.next())
        {
            res.add(Oeuvre.get(rs.getInt(1),con));
        }
        return res;
    }

    public ObservableList<Oeuvre> rechercheResume(String text) throws SQLException {
        PreparedStatement stmt = con.prepareStatement("SELECT id FROM oeuvre WHERE synopsis LIKE ?");
        stmt.setString(1,"%"+text+"%");
        ResultSet rs = stmt.executeQuery();
        ObservableList<Oeuvre> res = FXCollections.observableArrayList();
        while(rs.next())
        {
            res.add(Oeuvre.get(rs.getInt(1),con));
        }
        return res;
    }

    public ObservableList<Oeuvre> rechercheEditeur(String text) throws SQLException {
        PreparedStatement stmt = con.prepareStatement("SELECT DISTINCT oeuvre_id FROM edition WHERE editeur LIKE ?");
        stmt.setString(1,"%"+text+"%");
        ResultSet rs = stmt.executeQuery();
        ObservableList<Oeuvre> res = FXCollections.observableArrayList();
        while(rs.next())
        {
            res.add(Oeuvre.get(rs.getInt(1),con));
        }
        return res;
    }

    public ObservableList<Edition> editions(Oeuvre oeuvre) throws SQLException {
        PreparedStatement stmt = con.prepareStatement("SELECT ISBN FROM edition NATURAL JOIN livre WHERE edition.id_oeuvre = ? and NOT EXISTS (SELECT 1 FROM emprunt WHERE emprunt.id_livre = livre.id AND emprunt.rendu IS null) ");
        stmt.setInt(1,oeuvre.getId());
        ResultSet rs = stmt.executeQuery();
        ObservableList<Edition> res = FXCollections.observableArrayList();
        while(rs.next())
        {
            res.add(Edition.get(rs.getString(1),con));
        }
        return res;
    }

    public boolean emprunter(Edition edition) throws SQLException {
        PreparedStatement stmt = con.prepareStatement("SELECT id FROM livre WHERE ISBN = ? AND not EXISTS (SELECT 1 FROM emprunt WHERE emprunt.id_livre = livre.id AND emprunt.rendu IS null) LIMIT 1");
        stmt.setString(1,edition.getIsbn());
        ResultSet rs = stmt.executeQuery();
        if(rs.next())
        {
            int idLivre = rs.getInt(1);
            PreparedStatement stmt_nbre = con.prepareStatement("SELECT count(*) FROM emprunt WHERE id_utilisateur = ? and rendu IS null");
            stmt_nbre.setInt(1,this.actif.getId());
            ResultSet rs_nbre = stmt.executeQuery();
            if (rs_nbre.next() && rs_nbre.getInt(1) < this.actif.getMaxEmprunt())
            {
                PreparedStatement stmt_update = con.prepareStatement("INSERT INTO emprunt (id_livre, id_utilisateur, debut, fin_prevue) values (?,?,CURRENT_DATE(),DATE_ADD(CURRENT_DATE(),INTERVAL ? DAY))");
                PreparedStatement stmt_getid = con.prepareStatement("SELECT LAST_INSERT_ID()");
                stmt_update.setInt(1,idLivre);
                stmt_update.setInt(2,this.actif.getId());
                stmt_update.setInt(3,this.actif.getDuree());
                stmt_update.executeUpdate();
                ResultSet rs_id = stmt_getid.executeQuery();
                rs_id.next();
                Emprunt.get(rs_id.getInt(1),con);
                return true;
            }
        }
        return false;
    }

    public ObservableList<User> rechercheUser(String text) throws SQLException {
        PreparedStatement stmt = con.prepareStatement("SELECT id FROM utilisateur WHERE CONCAT(prenom, ' ', nom ) LIKE ?");
        stmt.setString(1,"%"+text+"%");
        ResultSet rs = stmt.executeQuery();
        ObservableList<User> res = FXCollections.observableArrayList();
        while(rs.next())
        {
            res.add(User.get(rs.getInt(1),con));
        }
        return res;
    }

    public void bannir(User selectedItem, LocalDate value,String motif) throws SQLException {
        PreparedStatement stmt_update = con.prepareStatement("INSERT INTO bans (id_banni, id_admin, debut, fin, motif) values (?,?,CURRENT_DATE(),?,?))");
        stmt_update.setInt(1,selectedItem.getId());
        stmt_update.setInt(2,actif.getId());
        stmt_update.setDate(3,Date.valueOf(value));
        stmt_update.setString(4,motif);
        stmt_update.executeUpdate();
        selectedItem.addBan(actif,value,motif);
    }

    public Connection getCon() {
        return con;
    }

    public ObservableList<Forfait> forfaits() throws SQLException {
        PreparedStatement stmt = con.prepareStatement("SELECT id FROM forfait");
        ResultSet rs = stmt.executeQuery();
        ObservableList<Forfait> res = FXCollections.observableArrayList();
        while(rs.next())
        {
            res.add(Forfait.get(rs.getInt(1),con));
        }
        return res;
    }

    public void addForfait(String nom, Integer duree, Integer nombre) throws SQLException {
        PreparedStatement stmt_update = con.prepareStatement("INSERT INTO forfait (nom, duree, nombre) values (?,?,?)");
        stmt_update.setString(1,nom);
        stmt_update.setInt(2,duree);
        stmt_update.setInt(3,nombre);
        stmt_update.executeUpdate();
        PreparedStatement stmt_getid = con.prepareStatement("SELECT LAST_INSERT_ID()");
        ResultSet rs_id = stmt_getid.executeQuery();
        rs_id.next();
        Forfait.get(rs_id.getInt(1),con);
    }
}
