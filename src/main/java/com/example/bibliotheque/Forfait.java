package com.example.bibliotheque;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class Forfait {
    private static HashMap<Integer,Forfait> loaded = new HashMap<>();
    private String nom;
    private int duree;
    private int nombre;
    private int id;
    public Forfait(int id, String nom, int duree, int nombre) {
        this.nom = nom;
        this.duree = duree;
        this.nombre = nombre;
        this.id = id;
    }

    public static Forfait get(int id, Connection con) throws SQLException {
        if (!loaded.containsKey(id))
        {
            PreparedStatement stmt = con.prepareStatement("Select nom, duree, nombre from forfait where id= ?");
            stmt.setInt(1,id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                loaded.put(id,new Forfait(id, rs.getString(1), rs.getInt(2), rs.getInt(3)));
            }
        }
        return loaded.get(id);
    }

    public int getMaxEmprunt() {
        return  this.nombre;
    }

    public int getDuree() {
        return  this.duree;
    }

    public String getNom() {
        return  this.nom;
    }

    @Override
    public String toString() {
        return this.nom;
    }

    public int getId() {
        return id;
    }
    public void modif(String nom, int duree, int simultane,Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement("UPDATE forfait SET nom = ?, duree = ?, nombre = ? WHERE id = ?");
        stmt.setInt(4,this.id);
        stmt.setString(1,nom);
        stmt.setInt(2, duree);
        stmt.setInt(3, simultane);
        stmt.executeUpdate();
        this.duree = duree;
        this.nom = nom;
        this.nombre = simultane;
    }
}
