package com.example.bibliotheque;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class User {
    private static HashMap<Integer,User> loaded = new HashMap<>();

    private String prenom ;
    private String nom;
    private String mail;
    private String genre;
    private Forfait forfait;
    private ArrayList<Ban> bans;
    private int id;
    private boolean biblio ;
    public User(int id, Forfait forfait, String prenom, String nom, String mail, boolean bibliothequaire, String genre, ArrayList<Ban> bans) {
        this.prenom = prenom;
        this.nom = nom;
        this.mail = mail;
        this.biblio = bibliothequaire;
        this.genre = genre;
        this.id = id;
        this.forfait = forfait;
        this.bans = bans;
    }

    public static User get(int id, Connection con) throws SQLException {
        if (!loaded.containsKey(id))
        {
            PreparedStatement stmt = con.prepareStatement("Select mail, prenom, nom, bibliothequaire, genre, id_forfait from utilisateur where id= ?");
            stmt.setInt(1,id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                Forfait f = null;
                if (rs.getInt(6) > 0)
                {
                    f = Forfait.get(rs.getInt(6),con);
                }
                PreparedStatement stmtbans = con.prepareStatement("Select id_admin, debut, fin, motif FROM bans WHERE id_banni = ?");
                stmtbans.setInt(1,id);
                ResultSet rsbans = stmtbans.executeQuery();
                ArrayList<Ban> bans = new ArrayList<>();
                while(rsbans.next())
                {
                    bans.add(new Ban(rsbans.getDate(2).toLocalDate(),rsbans.getDate(3).toLocalDate(),rsbans.getString(4),User.get(rsbans.getInt(1),con)));
                }
                User temp = new User(id, f, rs.getString(2), rs.getString(3), rs.getString(1), rs.getBoolean(4), rs.getString(5),bans);
                loaded.put(id,temp);
            }
        }
        return loaded.get(id);
    }

    public int getId() {
        return id;
    }
    public boolean getAdmin()
    {
        return biblio;
    }

    public int getMaxEmprunt() {
        if (forfait == null)
        {
            return 0;
        }
        return this.forfait.getMaxEmprunt();
    }

    public int getDuree() {
        if (forfait == null)
        {
            return 0;
        }
        return  this.forfait.getDuree();
    }

    public String getNomStr() {
        return this.prenom + " " + this.nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public String getNom() {
        return this.nom;
    }

    public String getBans() {
        String res = "";
        for(Iterator<Ban> iter = this.bans.iterator(); iter.hasNext();)
        {
            res += iter.next().getBans()
        }
        return res
    }

    public String getForfait() {
        return this.forfait.getNom();
    }

    public void addBan(User actif, LocalDate value, String motif) {
        bans.add(new Ban(LocalDate.now(),value,motif,actif));
    }
    public void setForfait(Forfait forfait, Connection con) throws SQLException {
        this.forfait = forfait;
        PreparedStatement stmt = con.prepareStatement("UPDATE utilisateur SET id_forfait = ? WHERE id = ?");
        stmt.setInt(1,forfait.getId());
        stmt.setInt(2,id);
        stmt.executeUpdate();
    }
}
