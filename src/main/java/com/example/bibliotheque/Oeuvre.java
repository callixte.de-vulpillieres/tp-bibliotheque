package com.example.bibliotheque;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Year;
import java.util.*;

public class Oeuvre {
    private static HashMap<Integer,Oeuvre> loaded = new HashMap<>();
    private int id;
    private String titre;
    private Year parution;
    private String synopsis;
    private String langue;
    private List<String> motsClefs;
    private List<Auteur> auteur;

    private Oeuvre(int id, String titre, Year parution, String synopsis, String langue, List<String> motsClefs, List<Auteur> auteur)
    {
        this.id = id;
        this.titre = titre;
        this.langue = langue;
        this.parution = parution;
        this.synopsis = synopsis;
        this.motsClefs = motsClefs;
        this.auteur = auteur;
    }
    public static Oeuvre get(int id, Connection con) throws SQLException {
        if (!loaded.containsKey(id))
        {
            PreparedStatement stmt = con.prepareStatement("Select titre, parution, synopsis, langue, motclef1, motclef2, motclef3, motclef4, motclef5 from oeuvre where id= ?");
            stmt.setInt(1,id);

            PreparedStatement stmtAuteurs = con.prepareStatement("Select id_auteur from ecrit where id_oeuvre = ?");
            stmtAuteurs.setInt(1,id);

            ResultSet rs = stmt.executeQuery();
            ResultSet rsAuteurs = stmtAuteurs.executeQuery();
            if (rs.next())
            {
                List<Auteur> auteur = new ArrayList<>();
                while(rsAuteurs.next())
                {
                    auteur.add(Auteur.get(rsAuteurs.getInt(1),con));
                }
                List<String> motsclefs = new ArrayList<>();
                for(int i = 0; i<5; i++)
                {
                    String mot = rs.getString(5+i);
                    if (mot != null){
                        motsclefs.add(mot);
                    }
                }
                loaded.put(id, new Oeuvre(id,rs.getString(1),Year.of(rs.getInt(2)),rs.getString(3),rs.getString(4),motsclefs,auteur));
            }
        }
        return loaded.get(id);
    }

    public String getTitre() {
        return this.titre;
    }

    public String getAuteurs() {
        String res = new String();
        for(Iterator<Auteur> iter = this.auteur.iterator(); iter.hasNext();)
        {
            res += iter.next().toString() + "; ";
        }
        return res.substring(0,res.length()-2);
    }

    public Year getAnnee() {
        return this.parution;
    }

    public String getLangue() {
        return this.langue;
    }

    public String getMotsClefs() {
        String res = new String();
        for(Iterator<String> iter = this.motsClefs.iterator(); iter.hasNext();)
        {
            res += iter.next() + "; ";
        }
        return res.substring(0,res.length()-2);
    }

    public String getSynopsis() {
        return this.synopsis;
    }

    public int getId() {
        return id;
    }
}
