package com.example.bibliotheque;

import javafx.beans.property.StringProperty;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Year;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Livre {
    private static HashMap<Integer,Livre> loaded = new HashMap<>();
    private int id;
    private Edition edition;
    public static Livre get(int id, Connection con) throws SQLException {
        if (!loaded.containsKey(id))
        {
            PreparedStatement stmt = con.prepareStatement("Select isbn from livre where id= ?");
            stmt.setInt(1,id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next())
            {
                loaded.put(id, new Livre(id,Edition.get(rs.getString(1),con)));
            }
        }
        return loaded.get(id);
    }
    private Livre(int id, Edition edition)
    {
        this.id = id;
        this.edition = edition;
    }
    public int getId() {
        return id;
    }
    public String getIsbn()
    {
        return edition.getIsbn();
    }

    public String getTitre() {
        return this.edition.getTitre();
    }

    public Year getAnnee() {
        return this.edition.getAnnee();
    }

    public String getEditeur() {
        return this.edition.getEditeur();
    }

    public String getAuteurs() {
        return this.edition.getAuteurs();
    }
}
